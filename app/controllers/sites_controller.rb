class SitesController < ApplicationController
  before_action :set_site, only: [:show, :edit, :update, :destroy, :assets]
  before_action :check_permissions

  # GET /sites
  # GET /sites.json
  def index
    @sites = Site.all
  end

  # GET /sites/1
  # GET /sites/1.json
  def show
  end

  # GET /sites/new
  def new
    @site = Site.new
  end

  # GET /sites/1/edit
  def edit
  end

  # POST /sites
  # POST /sites.json
  def create
    @site = Site.new(site_params)

    respond_to do |format|
      if @site.save
        format.html { redirect_to @site, notice: 'Site was successfully created.' }
        format.json { render :show, status: :created, location: @site }
      else
        format.html { render :new }
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sites/1
  # PATCH/PUT /sites/1.json
  def update
    respond_to do |format|
      if @site.update(site_params)
        format.html { redirect_to @site, notice: 'Site was successfully updated.' }
        format.json { render :show, status: :ok, location: @site }
      else
        format.html { render :edit }
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sites/1
  # DELETE /sites/1.json
  def destroy
    @site.destroy
    respond_to do |format|
      format.html { redirect_to sites_url, notice: 'Site was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def assets
    @current_user = current_user
  end

  def save_assets
    @i = 0
    @site = Site.find(params[:site_id])
    @site.site_assets.each do |asset|
      asset.destroy
    end

    require 'open-uri'
    params['assets'].each do |asset|
      unless asset["asset_url"].blank?
        @site_asset = SiteAsset.new(
            :asset_url => asset["asset_url"],
            :site_id => asset["site_id"]
        )
        @site_asset.save
        download = open(asset["asset_url"])
        IO.copy_stream(download, "#{Rails.root}/public#{download.base_uri.path.to_s}")
      end
    end

    redirect_to(sites_path)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site
      @site = Site.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def site_params
      params.require(:site).permit(:name)
    end

    def check_permissions
      unless current_user && current_user.can("manage_sites")
        flash[:notice] = "You must login to view this Page"
        redirect_to(login_path)
      end
    end
end
