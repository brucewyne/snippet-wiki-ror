class LoginController < ApplicationController
  def index
  end

  def login
    if params[:email].present? && params[:password].present?
      found_user = User.where(:email => params[:email]).first
      if found_user
        if found_user.active
          authorized_user = found_user.authenticate(params[:password])
        else
          flash[:notice] = "This user account isn't active yet."
          render('index')
          return false
        end
      end
    end

    if authorized_user
      session[:user_id] = authorized_user.id
      flash[:notice] = "you are now logged in."
      redirect_to('/')
    else
      flash.now[:notice] = "Invalid username/password combination."
      render('index')
    end
  end

  def logout
    session[:user_id] = nil
    flash[:notice] = "You are now logged out"
    redirect_to('/')
  end

  def register
  end
end
