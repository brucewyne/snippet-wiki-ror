class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :current_user
  helper_method :current_site

  private

  def current_user
    User.find(session[:user_id]) if session[:user_id] && User.exists?(id: session[:user_id])
  end

  def current_site
    unless cookies[:site]
      return Site.all.first
    else
      if Site.exists?(id: cookies[:site])
        return Site.find(cookies[:site])
      else
        return Site.all.first
      end
    end
  end

end
