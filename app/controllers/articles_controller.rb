class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy,:blocks]
  before_action :check_permissions, only: [:new, :edit, :blocks]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @author = User.find(@article.author)
    @last_author = User.find(@article.author)
  end

  # GET /articles/new
  def new
    @article = Article.new
    @categories = ArticleCategory.all
  end

  # GET /articles/1/edit
  def edit
    @categories = ArticleCategory.all
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)
    @categories = ArticleCategory.all
    @category = ArticleCategory.find(article_params[:article_category_id])
    respond_to do |format|
      if @article.save
        format.html { redirect_to "/articles/"+@article.id.to_s+"/blocks", notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    @categories = ArticleCategory.all
    @category = ArticleCategory.find(article_params[:article_category_id])
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to "/articles/"+@article.id.to_s+"/blocks", notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def blocks
    @current_user = current_user
  end
  
  def save_blocks
    @i = 0
    @article = Article.find(params[:article_id])
    @article.article_blocks.each do |block|
      block.destroy
    end

    params['blocks'].each_with_index do |block,index|
      @article_block = ArticleBlock.new(
        :block_type => block["block_type"],
        :display => block["display"],
        :content => block["content"],
        :order => block["order"],
        :author => block["author"],
        :last_edited_author => block["last_edited_author"],
        :article_id => block["article_id"]
      )
      @article_block.save
    end
    redirect_to(@article)
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:name, :author, :last_edited_author, :article_category_id)
    end

    def check_permissions
      unless current_user && current_user.can("add_articles")
        flash[:notice] = "You must login to add an Article"
        redirect_to(login_path)
      end
    end
end
