class ArticleBlocksController < ApplicationController
  before_action :set_article_block, only: [:show, :edit, :update, :destroy]

  # GET /article_blocks
  # GET /article_blocks.json
  def index
    @article_blocks = ArticleBlock.all
  end

  # GET /article_blocks/1
  # GET /article_blocks/1.json
  def show
  end

  # GET /article_blocks/new
  def new
    @article_block = ArticleBlock.new
  end

  # GET /article_blocks/1/edit
  def edit
  end

  # POST /article_blocks
  # POST /article_blocks.json
  def create
    @article_block = ArticleBlock.new(article_block_params)

    respond_to do |format|
      if @article_block.save
        format.html { redirect_to @article_block, notice: 'Article block was successfully created.' }
        format.json { render :show, status: :created, location: @article_block }
      else
        format.html { render :new }
        format.json { render json: @article_block.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /article_blocks/1
  # PATCH/PUT /article_blocks/1.json
  def update
    respond_to do |format|
      if @article_block.update(article_block_params)
        format.html { redirect_to @article_block, notice: 'Article block was successfully updated.' }
        format.json { render :show, status: :ok, location: @article_block }
      else
        format.html { render :edit }
        format.json { render json: @article_block.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /article_blocks/1
  # DELETE /article_blocks/1.json
  def destroy
    @article_block.destroy
    respond_to do |format|
      format.html { redirect_to article_blocks_url, notice: 'Article block was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article_block
      @article_block = ArticleBlock.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_block_params
      params.require(:article_block).permit(:block_type, :content, :order, :author, :last_edited_author)
    end
end
