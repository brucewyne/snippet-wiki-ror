json.extract! article_category, :id, :name, :handle, :site, :created_at, :updated_at
json.url article_category_url(article_category, format: :json)
