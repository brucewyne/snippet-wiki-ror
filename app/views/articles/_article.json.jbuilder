json.extract! article, :id, :name, :author, :last_edited_author, :article_category, :created_at, :updated_at
json.url article_url(article, format: :json)
