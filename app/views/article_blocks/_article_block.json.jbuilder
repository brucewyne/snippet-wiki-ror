json.extract! article_block, :id, :block_type, :content, :order, :author, :last_edited_author, :created_at, :updated_at
json.url article_block_url(article_block, format: :json)
