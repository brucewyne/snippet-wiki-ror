$(function(){
    function toggleSaveButton() {
        if ($("#site-assets-container .site-asset").length)
        {
            $(".bttn-save-assets").removeClass("hide");
        } else {
            $(".bttn-save-assets").addClass("hide");
        }
    }
    $("[data-add-asset]").click(function(e){
        e.preventDefault();
        var assetTemplate = _.template($("#asset-template").html());
        $("#site-assets-container").append(assetTemplate);
        toggleSaveButton();
    });

});