var Block = {
    type: null,
    id: 0,
    display: false,
    init: function(blockType)
    {
        this.type = blockType;
        if (blockType=="html-display") {
            this.type = "html";
            this.display = true;
        }
        return this;
    },
    treatText: function(text)
    {
      if (text=="html"){
          return "HTML";
      }  
      if (text=="javascript"){
          return "Javascript";
      }
      if (text=="css")
      {
          return "CSS";
      }
      if (text=="content")
      {
          return "Content";
      }
      return text;
    },
    addBlock: function()
    {
        this.id = Math.floor((Math.random() * 99999) + 100);
        if (this.type == "content") {
            this.addContentBlock();
        } else {
            this.addCodeBlock();
        }
        this.toggleBlockSaveButton();
        this.index();
        return this;
    },
    addCodeBlock: function()
    {
        var randomNumber = Math.floor((Math.random() * 99999) + 100);
        var params = {
            idNumber: this.id,
            blockName: this.treatText(this.type) + " Block",
            blockType: this.type
        };
        var template = _.template($("#code-block-template").html());
        $("#article-blocks-container").append(template(params));
        this.initAce(true);
        this.initDragNDrop();
        return this;
    },
    addContentBlock: function()
    {
        var randomNumber = Math.floor((Math.random() * 99999) + 100);
        var params = {
            idNumber: this.id,
            blockName: this.treatText(this.type) + " Block",
            blockType: this.type
        };
        var template = _.template($("#content-block-template").html());
        $("#article-blocks-container").append(template(params));
        this.initContentEditor(true);
        this.initDragNDrop();
        return this;
    },
    removeBlock: function(id)
    {
        $("#article-block-"+id).remove();
        this.toggleBlockSaveButton();
        this.index();
    },
    initContentEditor: function(editable)
    {
        $('.content-editor').trumbowyg({
            autogrow: true
        });
        if (!editable) {
            $('.content-editor').trumbowyg('disable');
        }
    },
    initAce: function(editable,type)
    {
        if (_.isUndefined(editable)) {
            editable = true;
        }
        if (_.isUndefined(type)) {
            type = this.type;
        }
        var self = this;
        $(".ace-editor").each(function(){
            var editorID = $(this).attr("id");
            var editor = ace.edit(editorID);
            var idNumber = editorID.replace("editor-","");
            if (type=="html") { var editorType = "coldfusion"; }
            if (type=="css") { var editorType = "scss"; }
            if (type=="javascript") { var editorType = "javascript"; }
            editor.setTheme("ace/theme/monokai");
            editor.getSession().setMode("ace/mode/"+editorType);
            editor.setAutoScrollEditorIntoView(true);
            editor.setOptions({
               maxlines: 50 
            });
            if (!editable) {
                editor.setReadOnly(true);
            }
            var editorHeight = editor.getSession().getScreenLength()
                * editor.renderer.lineHeight
                + editor.renderer.scrollBar.getWidth();
            $("#"+editorID).height(editorHeight.toString() + "px");
            editor.resize();
            $("#textarea-"+idNumber).val(editor.getSession().getValue());
            editor.getSession().on("change", function(){
                var editorHeight = editor.getSession().getScreenLength()
                  * editor.renderer.lineHeight
                  + editor.renderer.scrollBar.getWidth();
                $("#"+editorID).height(editorHeight.toString() + "px");
                editor.resize();
                $("#textarea-"+idNumber).val(editor.getSession().getValue());
            });
        });
    },
    initDragNDrop: function()
    {
        $("#article-blocks-container").sortable({
            handle: ".fa-arrows",
            update: function(){
                //nada
            },
            start: function(event,ui) {
                ui.item.addClass('is-being-dragged');
            },
            stop: function(event,ui) {
                ui.item.removeClass('is-being-dragged');
            }
        });

    },
    index: function()
    {
        $(".article-block").each(function(index){
            $(this).find(".block-order").val(index);
        });
    },
    toggleBlockSaveButton: function()
    {
        if ($("#article-blocks-container .article-block").length)
        {
            $(".bttn-save-blocks").removeClass("hide");
        } else {
            $(".bttn-save-blocks").addClass("hide");
        }
    }
};

$(function(){
    $("[data-add-block]").click(function(e){
        e.preventDefault();
        var blockType = $(this).attr("data-add-block");
        var block = Block.init(blockType);
        block.addBlock();
    });
    $(document).on("click","[data-delete-block]", function(){
        var id = $(this).attr("data-delete-block");
        Block.removeBlock(id);
    });
    $("[data-block-type='html'],[data-block-type='css'],[data-block-type='html']").each(function(){
        var blockType = $(this).attr("data-block-type");
        var editable = $(this).attr("data-editable");
        Block.initAce(editable, blockType);
    });
    $("[data-block-type='content']").each(function(){
        var blockType = $(this).attr("data-block-type");
        var editable = $(this).attr("data-editable");
        Block.initContentEditor(editable);
    });
    Block.toggleBlockSaveButton();
    Block.initDragNDrop();
});