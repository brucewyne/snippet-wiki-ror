// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery_ujs
// require turbolinks
//= require lib/underscore
//= require cable
//= require utilities
//= require lib/trumbowyg.min.js
//= require blocks
//= require assets
//= require application

_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

$(function(){
    $("#site-selector-field").change(function(){
        Utilities.setCookie("site",$(this).find("option:selected").val(),30);
        location.reload();
    });
    $(".bttn.dropdown").click(function(e){
       e.preventDefault();
        $(this).parent().toggleClass("open");
    });
    $(".dropdown-menu a").click(function(e){
       e.preventDefault();
        //blah blah
        $(this).closest(".bttn-group").toggleClass("open");
    });
});
