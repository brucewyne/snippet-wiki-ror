class User < ApplicationRecord
  has_secure_password

  has_and_belongs_to_many :user_groups

  EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i

  validates :first_name,
            :presence => true,
            :length => { :maximum => 25 }
  validates :last_name,
            :presence => true,
            :length => { :maximum => 50 }
  validates :email,
            :presence => true,
            :length => { :maximum => 50 },
            :uniqueness => true,
            :format => { :with => EMAIL_REGEX },
            :confirmation => true

  def get_full_name
    "#{first_name} #{last_name}"
  end

  def get_short_name
    "#{first_name} #{last_name.chars.first}."
  end

  def can(permission_handle)
    Permission.find_by(:handle => permission_handle).has_permission(self)
  end

  def has_admin_access?
    self.can("manage_sites") ||
    self.can("manage_users") ||
    self.can("manage_user_groups") ||
    self.can("manage_permissions")
  end

end
