class Permission < ApplicationRecord
  
  has_and_belongs_to_many :user_groups
  
  validates :name,
            :presence => true,
            :length => { :maximum => 50 },
            :uniqueness => true
  validates :handle,
            :presence => true,
            :length => { :maximum => 50 },
            :uniqueness => true          

  def has_permission(user)
    @user_has_permission = false
    @intersection = self.user_groups.all & user.user_groups.all
    return !@intersection.empty?
  end
end
