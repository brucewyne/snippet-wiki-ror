class ArticleCategory < ApplicationRecord
  belongs_to :site
  has_many :articles
end
