class Site < ApplicationRecord
  has_many :article_categories
  has_many :site_assets
end
