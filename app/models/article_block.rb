class ArticleBlock < ApplicationRecord
  belongs_to :article

  def block_type_name
    text = self.block_type
    if text=="html"
      return "HTML"
    elsif text=="javascript"
      return "Javascript"
    elsif text=="css"
      return "CSS"
    elsif text=="content"
      return "Content"
    end
    return text
  end
end
