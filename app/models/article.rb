class Article < ApplicationRecord
  belongs_to :article_category
  has_many :article_blocks

  scope :by_recently_updated, lambda { order("updated_at DESC")}
end
