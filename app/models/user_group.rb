class UserGroup < ApplicationRecord
  has_and_belongs_to_many :users
  has_and_belongs_to_many :permissions

  scope :sorted, lambda { order("name ASC")}
  scope :newest_fist, lambda { order("created_at DESC")}
  scope :search, lambda { |query| where(["name LIKE ?", "%#{query}"])}

  validates :name,
            :presence => true,
            :length => { :maximum => 50 },
            :uniqueness => true

end
