class ApplicationMailer < ActionMailer::Base
  default from: 'codewiki@gmail.com'
  layout 'mailer'
end
