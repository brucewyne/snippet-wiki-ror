class UserMailer < ApplicationMailer
  def invite_email(user)
    @user = user
    @url = "http://localhost:3000/login"
    mail(to: @user.email, subject: "You've been invited to CodeWiki")
  end
end
