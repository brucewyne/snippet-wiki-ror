window.JF = window.JF || {}
JF.serializeForm = function(aFormID) {
	var postFields = {}
	var formObj
	if (typeof aFormID == "string")
		formObj = $("#" + aFormID + " :input")
	else
		formObj = $(aFormID).find(":input")
	$(formObj).not(".nopost").each(function(index, item) {
		if ($(item).attr("type") == "checkbox") {
			if ($("#" + $(item).prop("id")).prop("checked"))
				postFields[$(item).attr("name")] = true
		} else if ($(item).attr("type") == "radio")
			postFields[$(item).attr("name")] = $("input[name=" + $(item).attr("name") + "]:checked").val()
		else if ($(item).find(":selected").length)
			postFields[$(item).attr("name")] = $(item).find(":selected").val()
		else
			postFields[$(item).attr("name")] = $(item).val()
	})
	return postFields
}

/* global.js */
$(document).ready(function() {
	$.ajaxSetup({
	    cache: false,
		data: {"united_ajax":"true"}
	});
	/*
		Many methods here were moved/duplicated to 'shell' and 'brand' to support mixed mode.
	*/
	$(document).foundation();
	$(document).on('click', '.reveal-trigger', function(e){
		e.preventDefault();
		var url = $(this).attr("href");
		if (url.startsWith("#")) {
			var $modal = $(url);
			$modal.foundation('open');
		} else {
			JF.UI.dimBackground(true,false);
			var $modal = $("#generic-reveal");
			var $modalContent = $modal.find('.reveal-content');
			$.ajax(url).done(function(response){
				JF.UI.undimBackground();
				$modalContent.html(response);
				$modal.foundation("open");
			});

		}
	});
	$(document).on('click', '[data-scroll-to]', function(e){
		e.preventDefault();
		var target = $(this).attr('data-scroll-to');
		var targetDistanceFromTop = $("#"+target).offset().top;
		var scrollToDistance = targetDistanceFromTop - 100;
		//scroll to 100px before the actual target to account for the sticky header.
		$(window).scrollTo(scrollToDistance,200);
	});

	$(".faux-radio .radio-button").click(function(){
		var $input = $(this).parent().find('input');
		//uncheck related radio buttons
		var $relatedRadios = $(".faux-radio input[name='" + $input.attr('name') + "']").parent();
		$relatedRadios.removeClass('selected');
		$relatedRadios.find("input").prop("checked", false);

		//change this ones status
		$(this).parent().addClass('selected');
		$input.prop("checked", true);
		$relatedRadios.trigger('change');
	});

	$(".reveal").each(function(){
		if ($(this).hasClass("reveal-light")) {
			$(this).closest('.reveal-overlay').addClass("reveal-overlay-light");
		}
	});
	
});

//this function is dogs---
function ajaxed_form_submit(submit_button, error_class, hide_submit_button, target) {
 	hide_submit_button = typeof(hide_submit_button) != 'undefined' ? hide_submit_button : true;
	var this_form = $(submit_button).parents('.ajaxed_form');
	var result_div = target ? $(target) : $(this_form).find('.form_submit_result');
	var loading_div = $(this_form).find('.loading');
	var error_class_sel = '.' + error_class;

	// Disable Submit Button
	if (hide_submit_button)
		if (JF.Util.testActivated('ff4093')) {
			$(submit_button).addClass('disabled');
		} else {
			$(submit_button).hide();
		}

	// Show Loading Div
	$(loading_div).show();

	if (!target)
		$(result_div).hide();

	// Remove Error Class
	$(this_form).find(error_class_sel).removeClass(error_class);

	var promise = $.post($(this_form).attr("action"), JF.serializeForm($(this_form)))
	promise.always(function() {
		$(loading_div).hide();
	});
	promise.done(function(data, status, xhr) {
			//omg what a hack
		if (data.indexOf("submit_error = 1") > 0) {
			$(submit_button).show().removeClass('disabled');
			$(result_div).html(data).show();
			$.event.trigger('ajaxed_form_error_complete');
		} else {
			$.event.trigger('ajaxed_form_success_complete');
			$(result_div).html(data).show();
			$(submit_button).removeClass('disabled');
		}
	});
	promise.fail(function(jqxhr, status, err, defaultMarkup) {
		$(submit_button).show().removeClass('disabled');
		$(result_div).html(defaultMarkup).show();
		$.event.trigger('ajaxed_form_error_complete');
	});
    /*$(this_form).ajaxSubmit({
	    target: 	result_div,
        success: 	function () {
			$(loading_div).hide();
			if (submit_error == 1) {
				$(submit_button).show();
				$(result_div).show();
				$.event.trigger('ajaxed_form_error_complete');
			}
			else
				$.event.trigger('ajaxed_form_success_complete');
		}
    });*/
}

//if that one was dogs--- then this one is cows---
function ajaxed_cs_form_submit (submit_button, error_class, product_id, hide_submit_button, target) {
 	hide_submit_button = typeof(hide_submit_button) != 'undefined' ? hide_submit_button : true;
	var this_form = $(submit_button).parents('.ajaxed_form');
    var result_div = target ? $(target) : $(this_form).find('.form_submit_result');
	var loading_div = $(this_form).find('.loading');
	var error_class_sel = '.' + error_class;

	if (hide_submit_button) {
		// Disable Submit Button
		$(submit_button).hide();
	}
	// Show Loading Div
	$(loading_div).show();

    // Hide Error Div
    if (!target)
        $(result_div).hide();

	// Remove Error Class
	$(this_form).find(error_class_sel).removeClass(error_class);

	var promise = $.post($(this_form).attr("action"), JF.serializeForm($(this_form)))
	promise.always(function() {
		$(loading_div).hide()
	})
	promise.done(function(data, status, xhr) {
		if (data.indexOf("submit_error = 1") > 0) {
			$(submit_button).show()
			$(result_div).html(data).show()
			$.event.trigger('ajaxed_form_error_complete_' + product_id)
		} else {
			$.event.trigger('ajaxed_form_success_complete_' + product_id)
			$(result_div).html(data)
		}
	})
	promise.fail(function(jqxhr, status, err, defaultMarkup) {
		$(submit_button).show()
		$(result_div).html(defaultMarkup).show()
		$.event.trigger('ajaxed_form_error_complete_' + product_id)
	})
    /*$(this_form).ajaxSubmit({
        target: 	result_div,
        success: 	function () {
						$(loading_div).hide();
						if(submit_error == 1){
							$(submit_button).show();
							$(result_div).show();
							$.event.trigger('ajaxed_form_error_complete_' + product_id);
						}
						else {
							$.event.trigger('ajaxed_form_success_complete_' + product_id);
						}
        			}
    });*/
}

//ajax submit for bundle productss
function ajaxed_submit_bundle (submit_button_list, error_class, target) {

 	var this_form = result_div = loading_div = product_id = "";
    var error_class_sel = '.' + error_class;
    var list_form = new Array();
 	var list_form_arr = new Array();

 	//consolidate form inputs
 	$.each(submit_button_list, function(key, value){
		hide_submit_button = typeof(hide_submit_button) != 'undefined' ? hide_submit_button : true;
 		this_form = $(this).parents('.ajaxed_form');
 		result_div = $(this_form).find('.form_submit_result');
 		loading_div = $(this_form).find('.loading');

 		if (hide_submit_button) {
			// Disable Submit Button
			$(this).hide();
		}

		// Show Loading Div
		$(loading_div).show();

		// Hide Error Div
		$(result_div).hide();

		// Remove Error Class
		$(this_form).find(error_class_sel).removeClass(error_class);

 		list_form_arr.push(JF.serializeForm(this_form));
 	});

 	//remove unecessary form data
 	$.each(list_form_arr, function(pkey, pvalue){
 		var list_form_tmp_obj = {};
 		$.each(pvalue, function(ckey, cvalue){
 			if((!$.isNumeric(ckey)) && (ckey != 'undefined')){
                list_form_tmp_obj[ckey] = cvalue;
 			}
 		});
 		list_form.push(list_form_tmp_obj);
 	});

	this_form = $(submit_button_list.last()).parents('.ajaxed_form');
	result_div = target ? $(target) : $(this_form).find('.form_submit_result');
  	product_id = $(submit_button_list.last()).attr('id').split('_')[1];

 	$.ajax({
  		method: "POST",
  		url: "/index.cfm?action=product.add_bundle_process",
  		data: { list_form: JSON.stringify(list_form)},
	}).done(function(data, status, xhr){
  		if (data.indexOf("submit_error = 1") > 0) {
			$(result_div).html(data).show()
			$.event.trigger('ajaxed_form_error_complete_' + product_id)
		} else {
			$(result_div).html(data)
			$.event.trigger('ajaxed_form_success_complete_' + product_id)
		}
	}).fail(function(jqxhr, status, err, defaultMarkup){
		$(result_div).html(defaultMarkup).show()
		$.event.trigger('ajaxed_form_error_complete_' + product_id)
	}).always(function(){
		$('#addToCartSubmitButtonBundle').show();
		$(".addItemToCart").show();
		$.each(submit_button_list, function(key, value){
			this_form = $(this).parents('.ajaxed_form');
			loading_div = $(this_form).find('.loading');
			$(loading_div).hide();
		});
	});
}

function analyticsEvent(category,action,opt_label,opt_value) {
	try {
			//this allows you to code sanely with an argument collection instead of having to send in and support an ever-expanding number of variables
		if (category === Object(category)) {
			dataLayer.push({ 'event': 'trackEvent', 'eventCategory': category.category || '', 'eventAction': category.action || '', 'eventLabel': category.label || "", 'eventValue': category.value || ""});
		} else {
			dataLayer.push({ 'event': 'trackEvent', 'eventCategory': category, 'eventAction': action, 'eventLabel': opt_label, 'eventValue': opt_value});
		}
	}  catch(err) {
		if (window.console) console.log("analytics push failed");
	}
}

function trackPageView(virtualURL){
	try{
		dataLayer.push({
			event:"trackPage",
			pageURL: virtualURL
		});
	}
	catch(err){
		if(window.console){
			console.log("trackPage push failed");
		}
	}
}
/* FF-3534 ReadyPulse UGC */
function readyPulseUGC(albumID, pageView, baseURL, sku) {
	try {
		// Initialize the API
		RP.init();

		// Instantiate the objects of RP.Album class for each
		// album that you are rendering
		if (pageView === 'pdp') {
			// Scope is specified as the second argument to the constructor.
			var ugcData = new RP.Collection(albumID, 'product_ids:'+ sku +'');
		} else {
			// There is no scope specified.
			var ugcData = new RP.Album(albumID);
		}

		// Wait for "ugcData" to be ready
		ugcData.onReady(function() {
			// Invoke the method on the album object you instantiated
			// during the initialization step. In this case, "ugcData"
			ugcData.getContent(function(obj) {
				// This callback is invoked once the JSON object is fetched
				// the argument obj contains the JSON object
				if (typeof obj.error === 'undefined' && obj !== null) {
					var keyArr = [], itemArr = [], contentArr = new Array();
					var start = '', next = '', prev = '';

					if (pageView === 'stylegallery') {
						// Get the data and insert to an array
						for (var i in obj.content) {
							contentArr[obj.content[i].uniq_id] = obj.content[i];
							itemArr[i] = obj.content[i].uniq_id;
						}

						// Append the Style Gallery images
						for (var i in contentArr) {
							$('.ugc-gallery .thumbs').append('<a href="javascript:void(0);" id="'+ contentArr[i].uniq_id +'" data-ga-category="stylegallery" data-ga-action="ugc" data-ga-label="galleryopenimage" class="ugc-lb-thumb"><img src="'+ contentArr[i].media.images[2].url +'" alt="" /></a>');
						}

						$('.ugc-gallery .thumbs .ugc-lb-thumb:nth-child(n+21)').hide();

						var ugcGallery = $('.ugc-gallery div.thumbs > a');
						$('.ugc-gallery .headings').css('margin-bottom', '20px');
						$('.ugc-gallery .loading').show();
						$(window).on('load', function() {
							$('.ugc-gallery .headings').css('margin-bottom', '0');
							$('.ugc-gallery .loading').hide();
							if (ugcGallery.length > 20) {
								$('.ugc-gallery .load-more').show();
							} else {
								$('.ugc-gallery .load-more').hide();
							}
							$('.ugc-gallery .thumbs').show();
						});

						// Toggle "Load More" button
						$(document).on('click', '.ugc-gallery .load-more', function(e) {
							e.preventDefault();
							$('.ugc-gallery .thumbs .ugc-lb-thumb:nth-child(n+21)').fadeToggle();
							$(this).hide();
						});
					}

					if (pageView === 'psstylist') {
						// Get the data and insert to an array
						$.each(obj.content, function (index, value) {
							contentArr[obj.content[index].uniq_id] = obj.content[index];
							itemArr[index] = obj.content[index].uniq_id;
							return index < 7;
						});

						// Append the PS Stylist images
						for(var i in contentArr) {
							$('.ugc .thumbs').append('<a href="javascript:void(0);" id="'+ contentArr[i].uniq_id +'" data-ga-category="Stylist LP Desktop" data-ga-action="UGC" data-ga-label="Gallery Open Image" class="ugc-lb-thumb"><img src="'+ contentArr[i].media.images[2].url +'" alt="" /></a>');
						}

						$('.ugc .loading').show();
						$(window).on('load', function() {
							$('.ugc .loading').hide();
							$('.ugc .thumbs').show();
						});
					}
					
					if (pageView === 'psstylist_bio') {
						// Randomly get the data and insert to an array
						while (keyArr.length < 9) {
							var randomNum = Math.floor(Math.random() * obj.content.length);
							var found = false;
							for (var i = 0; i < keyArr.length; i++) {
								if (keyArr[i] == randomNum) {
									found = true;
									break;
								}
							}
							if (!found) {
								keyArr[keyArr.length] = randomNum;
								contentArr[obj.content[keyArr[i]].uniq_id] = obj.content[keyArr[i]];
								itemArr[i] = obj.content[keyArr[i]].uniq_id;
							}
						}

						// Append the Boutique images
						for(var i in contentArr) {
							$('.ugc .thumbs').append('<a href="javascript:void(0);" id="'+ contentArr[i].uniq_id +'" data-ga-category="Stylist Bio Desktop" data-ga-action="UGC" data-ga-label="Gallery Open Image" class="ugc-lb-thumb"><img src="'+ contentArr[i].media.images[2].url +'" alt="" /></a>');
						}

						// Wrap the images with div "column" class
						var ugcPSStylist = $('.ugc div.thumbs > a');
						for(var i = 0; i < ugcPSStylist.length; i+=3) {
							ugcPSStylist.slice(i, i+3).wrapAll('<div class="column"></div>');
						}

						$('.ugc .loading').show();
						$(window).on('load', function() {
							$('.ugc .loading').hide();
							$('.ugc .thumbs').show();
						});
					}

					if (pageView === 'boutique') {
						// Randomly get the data and insert to an array
						while (keyArr.length < 9) {
							var randomNum = Math.floor(Math.random() * obj.content.length);
							var found = false;
							for (var i = 0; i < keyArr.length; i++) {
								if (keyArr[i] == randomNum) {
									found = true;
									break;
								}
							}
							if (!found) {
								keyArr[keyArr.length] = randomNum;
								contentArr[obj.content[keyArr[i]].uniq_id] = obj.content[keyArr[i]];
								itemArr[i] = obj.content[keyArr[i]].uniq_id;
							}
						}

						// Append the Boutique images
						for(var i in contentArr) {
							$('.ugc .thumbs').append('<a href="javascript:void(0);" id="'+ contentArr[i].uniq_id +'" data-ga-category="boutique" data-ga-action="ugc" data-ga-label="boutiqueopenimage" class="ugc-lb-thumb"><img src="'+ contentArr[i].media.images[2].url +'" alt="" /></a>');
						}

						// Wrap the images with div "column" class
						var ugcBoutique = $('.ugc div.thumbs > a');
						for(var i = 0; i < ugcBoutique.length; i+=3) {
							ugcBoutique.slice(i, i+3).wrapAll('<div class="column"></div>');
						}

						$('.ugc .loading').show();
						$(window).on('load', function() {
							$('.ugc .loading').hide();
							$('.ugc .thumbs').show();
						});
					}

					if (pageView === 'pdp') {
						// Get the data and insert to an array
						for(var i in obj.content) {
							contentArr[obj.content[i].uniq_id] = obj.content[i];
							itemArr[i] = obj.content[i].uniq_id;
						}

						// Show/hide no content section
						if (obj.content.length === 0) {
							$('.ugc-with-content').hide();
							$('.ugc-no-content').show();
						} else {
							$('.ugc-with-content').show();
							$('.ugc-no-content').hide();
						}

						// Insert newly added slide
						if (JF.Util.testActivated("ff4093")) {
							var totalUGCImages = 0;
							$.each(obj.content, function (index, value) {
								var ugcSlideTemplate = _.template($("#ugc-swiper-slide-template").html());
								var data = {
									imageID: value.uniq_id,
									imagePath: value.media.images[2].url
								};
								JF.UI.pushDataToTemplate("#ugc-swiper .swiper-wrapper", ugcSlideTemplate, data);
								totalUGCImages = index;
							});
							var UGCSwiper = new Swiper("#ugc-swiper", {
								slidesPerView: "auto",
								spaceBetween: 10,
								loop: false,
								nextButton: '.ugc-image-next',
								prevButton: '.ugc-image-prev'
							});
						} else {
							$.each(obj.content, function (index, value) {
								$('.ugc .ugc-scroll .items').append('<a href="javascript:void(0);" id="' + value.uniq_id + '" data-ga-category="pdp" data-ga-action="old-ugc" data-ga-label="openimage" class="ugc-lb-thumb"><img src="' + value.media.images[2].url + '" alt="" /></a>');
							});
							// Wrap the images with div "slide" class
							var ugcPDP = $('.ugc div.items > a');
							for(var i = 0; i < ugcPDP.length; i+=5) {
								ugcPDP.slice(i, i+5).wrapAll('<div class="slide"></div>');
							}

							$('.ugc .ugc-scroll .items').cycle({next:'.ugc .ugc-scroll .next', prev:'.ugc .ugc-scroll .prev', timeout:0}).cycle('pause');

							$('.ugc .loading').show();
							$(window).on('load', function() {
								$('.ugc .loading').hide();
								$('.ugc .ugc-scroll .items').show();
							});
						}

						$(document).on('click', '.single .shot', function(e) {
							var catalogID = $(this).attr('id').split('go_link_')[1];
							if (sku !== '' && sku.indexOf(catalogID) !== -1) {
								e.preventDefault();
								$.colorbox.close();
								$(window).scrollTop(0);
							}
						});
					}

					function updateContentLB(id) {
						var largeImage = contentArr[id].media.images[4].url;
						var profileImage = contentArr[id].actor.image_url;
						var contentSource = contentArr[id].content_source;
						var name = contentArr[id].actor.handle_name;
						var date = contentArr[id].social_timestamp;
						var likes = contentArr[id].social_attributes.Likes;
						var comments = contentArr[id].social_attributes.Comments;
						var products = contentArr[id].products;

						start = id;
						next = itemArr[($.inArray(start, itemArr) + 1) % itemArr.length];
						prev = itemArr[($.inArray(start, itemArr) - 1 + itemArr.length) % itemArr.length];

						// Update the content on the popup
						$('#ugc_lb .large-image').attr('src', largeImage.replace('b_white', 'b_black')).css('display', 'none');
						if (typeof profileImage !== 'undefined' && profileImage !== null) {
							$('#ugc_lb .profile-image').attr('src', profileImage).css({'width':'40px', 'height':'40px', 'border-radius':'50%', 'display':'inline-block'});
						} else {
							$('#ugc_lb .profile-image').css('display', 'none');
						}
						if (typeof contentSource !== 'undefined' && contentSource !== null) {
							if (contentSource !== 'instagram') {
								$('#ugc_lb .name strong').html(name);
							} else {
								$('#ugc_lb .name strong').html('@'+ name);
							}
						}
						$('#ugc_lb .date').html(date);
						if (typeof likes !== 'undefined' && likes !== null) {
							$('#ugc_lb .instagram-likes').html(likes);
						} else {
							$('#ugc_lb .instagram-likes').html(0);
						}
						if (typeof comments !== 'undefined' && comments !== null) {
							$('#ugc_lb .instagram-comments').html(comments);
						} else {
							$('#ugc_lb .instagram-comments').html(0);
						}
						$('#ugc_lb .scroll .items').html('');
						if (typeof products !== 'undefined' && products !== null) {
							// Append "Related Products" images
							var slideHTML = '';
							var pageLabel = '';
							if (pageView === 'stylegallery') {
								pageLabel = 'gallery';
							} else {
								pageLabel = pageView;
							}
							$.each(products, function(index, value){
								var url = value.url.split('?ru=http://www.justfab.com/')[1];
								slideHTML += '<div class="single" style="display:none;">';
								slideHTML += '<a href="'+ baseURL + url +'&psrc=product_detail_related_items_ugc" class="shot" id="go_link_'+ value.catalog_id +'" data-ga-category="'+ pageView +'" data-ga-action="ugc" data-ga-label="'+ pageLabel +'related'+ (index+1) +'" rel="external">';
								slideHTML += '<img alt="" src="'+ value.image +'">';
								slideHTML += '<div class="name">'+ value.name.substring(0,40) +'</div>';
								slideHTML += '<div class="shop"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Shop Now</div>';
								slideHTML += '</a>';
								slideHTML += '</div>';
								return index < 1;
							});
							$('#ugc_lb .scroll .items').append(slideHTML);

							// Wrap the images with div "slide" class
							var ugcLB = $('#ugc_lb div.items .single');
							for(var i = 0; i < ugcLB.length; i+=2) {
								ugcLB.slice(i, i+2).wrapAll('<div class="slide"></div>');
							}

							if (products.length !== 0) {
								$('#ugc_lb .scroll .items').cycle({next:'#ugc_lb .scroll .next', prev:'#ugc_lb .scroll .prev', timeout:0}).cycle('pause');
							}
						}

						$('#ugc_lb .loading').show();
						$('#ugc_lb .large-image').on('load', function() {
							$(this).show();
							$('#ugc_lb .loading').hide();
							$('#ugc_lb .single').show();
						});
					}

					$(document).on('click', '.ugc-lb-thumb', function(e) {
						e.preventDefault();
						var id = $(this).attr('id');
						var products = contentArr[id].products;
						if (obj.content.length === 1) {
							$('#ugc_lb .icon-angle-left, #ugc_lb .icon-angle-right').hide();
						}
						if (products === null) {
							$('#ugc_lb .products .scroller-arrow-left, #ugc_lb .products .scroller-arrow-right').hide();
						}
						updateContentLB(id);
						$.colorbox({inline: true, href: '#ugc_lb', width: '1001px'});
					});

					$(document).on('click', '#ugc_lb a.fontello', function(e) {
						e.preventDefault();
						if ($(this).hasClass('icon-angle-left')) {
							updateContentLB(prev);
						} else {
							updateContentLB(next);
						}
					});

					$(document).keydown(function(e) {
						if(e.keyCode == 37) {
							updateContentLB(prev);
						}
						else if(e.keyCode == 39) {
							updateContentLB(next);
						}
					});
				} else {
					$('.ugc, .pdp-ugc').hide();
					if(window.console) {
						console.log('ReadyPulse UGC: '+ obj.error);
					}
				}
			});
		});
	}
	catch(err) {
		$('.ugc').hide();
		if(window.console) {
			console.log('ReadyPulse UGC failed');
		}
	}
}

var product_impression_element = $('div.item[data-ga-product-info]').attr("id");
var impressions_position_number = 1;
var sent_impressions = [];

function isElementInViewport(el){
	var rect = el.getBoundingClientRect();
	return(
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) + rect.height &&
		rect.right <= (window.innerWidth || document.documentElement.clientWidth) + rect.width
	);
}


$(window).on("DOMContentLoaded loaded resize scroll", function(){
	var ecommerce = new Array();
		/*
			Do not remove this try/catch.
			This is here because this single function can destroy any page in the site thanks to the events it's bound to.
			If you think you need to remove it for some reason, talk to Nick.
		*/
	$.each($('div.item[data-ga-product-info]'), function(index, item){
		try {
			if( (isElementInViewport(item) && $(item).parents("div[role=tabpanel]").attr("aria-hidden") === "false") || ( isElementInViewport(item) && $(item).is(':visible')) ){
				var product_info = $.parseJSON($(item).attr("data-ga-product-info"));
				product_info.price = product_info.price.toFixed(2);
				product_info.name = decodeURIComponent(product_info.name);
				product_info.variant = decodeURIComponent(product_info.variant);
				// The position starts at 1
				product_info.position = impressions_position_number;
				//check if we have already sent the product
				if($.inArray(product_info.id, sent_impressions) == -1){
					// Push to the array
					ecommerce.push(product_info);
					//save the array so we dont send duplicates
					sent_impressions.push(product_info.id);
					// increase the property number key
					impressions_position_number++;
				}
			}
		} catch (e) {
			if (window.console) console.log("error trying to push GA product info JSON to ecommerce array")
		}
	});
	if(ecommerce.length > 0){
		dataLayer.push({"ecommerce":{"impressions": ecommerce}
		,
			'event' : 'trackEvent',
			'eventCategory' : 'Ecommerce',
			'eventAction' : 'Impression'
		});
	}
});


function publish_fb (user_message, caption, name, description, image_src, href_url, object_type, link_text) {
	object_type_save = object_type;
	 FB.ui(
	   {
	     method: 'stream.publish',
	     message: user_message,
	     attachment: {
	       name: name,
	       caption: caption,
	       description: (
	         description
	       ),
	       href: href_url,
		   media:[{'type':'image','src':image_src,'href':href_url}]
	     },
	     action_links: [
	       { text: link_text, href: href_url }
	     ],
	     user_message_prompt: 'What\'s on your mind?'
	   },
	   record_publish_callback
	 );
}

function share_fb (user_message, caption, name, description, image_src, href_url, object_type, link_text) { // slightly different than publish_fb, pulled from legacy, currently used on share functionality on mp4/youtube swap
	object_type_save = object_type;
	FB_Args = {
		method: 'share',
		href: href_url,
		quote: caption,
		caption: caption,
		description: description,
		message: user_message,
		name: name,
		image: image_src,
		object_type: object_type
	};

	FB.ui( FB_Args, record_publish_callback);

}

function record_publish_callback (response) {
		if(response && response.post_id) {
			record_publish();
		}
}
function record_publish () {
	analyticsEvent('social', 'share', object_type_save);
}

function popup(url) {
	newwindow=window.open(url, "name", "height=350,width=600");
	if (window.focus) {newwindow.focus()}
	return false;
}
