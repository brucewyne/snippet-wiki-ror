JF.UI = {
	/*
	 * @param {String} target - Element Selector to append the template to
	 * @param {Object} template - Underscore Template
	 * @param {Object} data - Properties for the template
	 */
	pushDataToTemplate: function(target,template,data){
		$(target).append(template(data));
	},
	/*
	 * @param {Boolean} showLoader - if true, will show spinning loader
	 */
	dimBackground: function(showLoader,closeable){
		if($('.whiteout-screen').length < 1){
			$("body").append('<div class="whiteout-screen">');
		}
		$(".whiteout-screen").addClass('dimmed');
		if(closeable===false){
			$(".whiteout-screen").addClass('not-closeable');
		}
		if(showLoader==true){
			$(".whiteout-screen").append('<div><i class="fontello icon-spinner icon-animate-spin"></i></div>');
		}
		$(".whiteout-screen:not(.not-closeable)").click(function(){
			JF.UI.undimBackground();
		});
		$(document).trigger("dimmed");
	},

	undimBackground: function(){
		$(".whiteout-screen").removeClass('dimmed');
		setTimeout(function(){
			$(".whiteout-screen").remove();
		},300);
		$(document).trigger("undimmed");
	}
};
