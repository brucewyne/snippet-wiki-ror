JF.Util = {
	isFuseaction: function(stringList) {
		var FAArray = stringList.split(",");
		var matchingFAs = 0;
		FAArray.forEach(function (fa) {
			if (fa.indexOf(JF.Environment.Fuseaction) > -1) {
				matchingFAs++;
			}
		});
		return matchingFAs > 0;
	},
	getParameterByName: function(name) {
		name = name.replace(/[\[\]]/g, "\\$&");
		var url = window.location.href;
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	},
	testActivated: function(test, groups) {
		test = test.toUpperCase();
		if (!JF.Tests || !JF.Tests[test]) {
			return false
		}
		if (groups) {
			var passes = 0;
			groups.split(",").forEach(function(group) {
				if (group.indexOf(JF.Tests[test].group) > -1) {
					passes++;
				}
			});
			return passes > 0;
		} else {
			return JF.Tests[test].group >= JF.Tests[test].minimum_activated_group;
		}
	}
};