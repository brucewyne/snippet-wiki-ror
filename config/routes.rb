Rails.application.routes.draw do


  root :to => 'page#index'

  get 'login', :to => "login#index"
  post 'login/login'
  get 'login/login', :to => "login#index"
  get 'login/logout'
  get 'login/register'
  resources :permissions
  resources :user_groups
  resources :users
  get 'articles/:id/blocks', :to => "articles#blocks"
  post 'articles/save_blocks', :to => "articles#save_blocks"
  resources :articles, :except => [:index]
  get 'articles', to: redirect("/")
  resources :article_categories
  resources :sites
  get 'sites/:id/assets', :to => "sites#assets"
  post 'sites/save_assets', :to => "sites#save_assets"
  resources :posts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
