class CreateArticleBlocks < ActiveRecord::Migration[5.0]
  def change
    create_table :article_blocks do |t|
      t.string :block_type
      t.text :content
      t.integer :order
      t.integer :author
      t.integer :last_edited_author
      t.references :article

      t.timestamps
    end
  end
end
