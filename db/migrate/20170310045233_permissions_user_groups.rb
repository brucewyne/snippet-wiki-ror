class PermissionsUserGroups < ActiveRecord::Migration[5.0]
  def change
     create_table :permissions_user_groups do |t|
      t.belongs_to :permission, index: true
      t.belongs_to :user_group, index: true
    end
  end
end
