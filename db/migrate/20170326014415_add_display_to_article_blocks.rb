class AddDisplayToArticleBlocks < ActiveRecord::Migration[5.0]
  def change
    add_column :article_blocks, :display, :boolean
  end
end
