class CreateSiteAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :site_assets do |t|
      t.string :asset_url
      t.integer :site_id

      t.timestamps
    end
  end
end
