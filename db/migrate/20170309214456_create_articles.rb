class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :name
      t.integer :author
      t.integer :last_edited_author
      t.references :article_category

      t.timestamps
    end
  end
end
