# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#### Start with some sites.
Site.all.each do |site|
  site.destroy
end
Site.create(name: "JustFab")
Site.create(name: "ShoeDazzle")

#### Create some User Groups
UserGroup.all.each do |usergroup|
  usergroup.destroy
end
admin_user_group = UserGroup.create(name: "Admins")
contributor_user_group = UserGroup.create(name: "Contributors")

#### Create the admin user
User.all.each do |user|
  user.destroy
end
User.create(
  email: "admin@justfab.com", 
  first_name: "Admin", 
  last_name: "Master", 
  password: "A!b2c3d4", 
  user_group_ids: [admin_user_group.id,contributor_user_group.id]
)

#### Setup the base permissions
Permission.all.each do |user|
  user.destroy
end
Permission.create(name: "Manage Users", handle: "manage_users", user_group_ids: [admin_user_group.id])
Permission.create(name: "Manage User Groups", handle: "manage_user_groups", user_group_ids: [admin_user_group.id])
Permission.create(name: "Manage Permissions", handle: "manage_permissions", user_group_ids: [admin_user_group.id])
Permission.create(name: "Manage Sites", handle: "manage_sites", user_group_ids: [admin_user_group.id])
Permission.create(name: "Create Categories", handle: "create_categories", user_group_ids: [contributor_user_group.id])
Permission.create(name: "Remove Categories", handle: "remove_categories", user_group_ids: [admin_user_group.id])
Permission.create(name: "Add Articles", handle: "add_articles", user_group_ids: [contributor_user_group.id])
Permission.create(name: "Remove Articles", handle: "delete_articles", user_group_ids: [admin_user_group.id])