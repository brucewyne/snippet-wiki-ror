require 'test_helper'

class ArticleBlocksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article_block = article_blocks(:one)
  end

  test "should get index" do
    get article_blocks_url
    assert_response :success
  end

  test "should get new" do
    get new_article_block_url
    assert_response :success
  end

  test "should create article_block" do
    assert_difference('ArticleBlock.count') do
      post article_blocks_url, params: { article_block: { author: @article_block.author, block_type: @article_block.block_type, content: @article_block.content, last_edited_author: @article_block.last_edited_author, order: @article_block.order } }
    end

    assert_redirected_to article_block_url(ArticleBlock.last)
  end

  test "should show article_block" do
    get article_block_url(@article_block)
    assert_response :success
  end

  test "should get edit" do
    get edit_article_block_url(@article_block)
    assert_response :success
  end

  test "should update article_block" do
    patch article_block_url(@article_block), params: { article_block: { author: @article_block.author, block_type: @article_block.block_type, content: @article_block.content, last_edited_author: @article_block.last_edited_author, order: @article_block.order } }
    assert_redirected_to article_block_url(@article_block)
  end

  test "should destroy article_block" do
    assert_difference('ArticleBlock.count', -1) do
      delete article_block_url(@article_block)
    end

    assert_redirected_to article_blocks_url
  end
end
